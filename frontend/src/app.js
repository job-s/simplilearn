import React, { useEffect, useState } from "react";

import Routes from "./configs/routes/routes";

const App = () => {
	const [authCheck, setAuthCheck] = useState(false);

	useEffect(() => {
		const uri = `http://localhost:9000/api/user/me`;
		const accessToken = localStorage.getItem("access_token");

		if (!accessToken) {
			setAuthCheck(true);
			return;
		}

		fetch(uri, {
			method: "GET",
			headers: {
				Authorization: `${accessToken}`,
				"Content-Type": "application/json",
			},
		})
			.then((response) => response.json())
			.then((res) => {
				if (res.error) {
					throw res.error;
				}
				setAuthCheck(true);
			})
			.catch((error) => {
				console.error(error);
				localStorage.removeItem("access_token");
				setAuthCheck(true);
			});
	}, []);

	return <>{authCheck ? <Routes></Routes> : "Loading..."}</>;
};

export default App;
