import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

const createRootReducer = (customHistory) =>
	combineReducers({
		// ...exampleReducer
		router: connectRouter(customHistory),
	});

export default createRootReducer;
