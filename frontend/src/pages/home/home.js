import React from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import "./home.scss";

const Home = ({ push }) => {
	const logout = () => {
		localStorage.removeItem("access_token");
		push("/auth");
	};

	return (
		<div className="home-page">
			<button onClick={logout}>Logout</button>
		</div>
	);
};

const enhance = connect(null, {
	push,
});

export default enhance(Home);
