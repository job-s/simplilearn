import React, { useRef, useState } from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import "./auth.scss";

const Auth = ({ push }) => {
	const email = useRef(null);
	const password = useRef(null);
	const [type, setType] = useState("login");
	const [error, setError] = useState("");

	const onClickSubmit = (event) => {
		event.preventDefault();

		const uri = `http://localhost:9000/api/auth/${type}`;

		fetch(uri, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email.current.value,
				password: password.current.value,
			}),
		})
			.then((response) => response.json())
			.then((res) => {
				if (res.error) {
					throw res.message;
				}

				localStorage.setItem("access_token", res.token);
				push("/home");
			})
			.catch((error) => {
				console.log(error);
				setError(error);
			});
	};

	const onToggle = (event) => {
		if (event.currentTarget.classList.contains("toggle-login")) {
			setType("login");
		} else {
			setType("signup");
		}
	};

	return (
		<div className="auth-page">
			<div>
				<div className="toggle-parent">
					<div
						className={`toggle-login-signup toggle-login ${
							type === "login" ? "toggle-active" : ""
						}`}
						onClick={onToggle}
					>
						Login
					</div>
					<div
						className={`toggle-login-signup toggle-signup ${
							type === "signup" ? "toggle-active" : ""
						}`}
						onClick={onToggle}
					>
						Signup
					</div>
				</div>
				<form>
					<input ref={email} type="email" />
					<input ref={password} type="password" />
					<button type="submit" onClick={onClickSubmit}>
						{type}
					</button>
					{error && <p>{error}</p>}
				</form>
			</div>
		</div>
	);
};

const enhance = connect(null, {
	push,
});

export default enhance(Auth);
