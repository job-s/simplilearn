import express from "express";
import bodyParser from "body-parser";
import { requestErrorHandler } from "../utils/requestErrorHandler";

const router = express.Router();
router.use(bodyParser.json());

router.get("/me", async (req, res) => {
	try {
		res.json({ email: req.user.email });
	} catch (error) {
		requestErrorHandler(res, error.message);
	}
});

export default router;
