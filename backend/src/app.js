import dotenv from "dotenv";
import express from "express";
import boom from "express-boom";
import cors from "cors";
import initMongoDB from "./services/mongodb";
import apiRouter from "./routes";
import { authMiddleware } from "./middlewares/authMiddleware";

dotenv.config();

const PORT = process.env.PORT || 9000;

const app = express();

app.use(boom());
app.use(cors());

app.use("/api", authMiddleware, apiRouter);

app.listen(PORT, () => {
	console.log("Listening on port: " + PORT);
});

initMongoDB();
