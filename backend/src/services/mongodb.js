import mongoose from "mongoose";
import { mongoDBOptions } from "../config";

export default () => {
	return mongoose
		.connect(process.env.mongoURI, mongoDBOptions)
		.then(() => {
			console.log("Connected to mongoDB!");
		})
		.catch(() => {
			console.log("Connection to mongoDB failed!");
		});
};
