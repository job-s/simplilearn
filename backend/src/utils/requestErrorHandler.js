export const requestErrorHandler = (res, error) => {
	if (error.type === "badRequest") {
		res.boom.badRequest(error.message);
	} else if (error.type === "badImplementation") {
		res.boom.badImplementation();
	} else {
		res.boom.badImplementation();
	}
};
