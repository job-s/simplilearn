class CustomError extends Error {
	constructor(type = "badRequest", ...params) {
		super(...params);

		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, CustomError);
		}

		this.type = type;
	}
}

export default CustomError;
