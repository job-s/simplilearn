export const mongoDBOptions = {
	useNewUrlParser: true,
	useUnifiedTopology: true,
};
